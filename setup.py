from setuptools import setup, find_packages

setup(
    name='pytorch-hymenoptera',
    version='0.1',
    description='none',
    license='MIT',
    install_requires=[
        'gitpython >= 3.0.2',
        'scipy >= 1.3',
    ],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    entry_points={
        'console_scripts': [
            # 'pytorch-hymenoptera = pytorch_hymenoptera:main',
            'pthym-script = pytorch_hymenoptera.script:main',
        ],
    },
)
