import numpy as np


def dvd(x, y):
    return np.float64(x) / y


def format_elapsed_time(t):
    return '{:.0f}d{:.0f}h{:.0f}m{:.0f}s'.format(
        t // 86400,
        t // 3600,
        t // 60,
        t % 60,
    )
