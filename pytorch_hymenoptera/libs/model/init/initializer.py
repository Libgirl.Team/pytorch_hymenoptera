from torchvision import models
import torch.nn as nn
from pytorch_hymenoptera.libs.model.builder import set_parameter_requires_grad


def resnet18(num_classes, feature_extract, use_pretrained=True):
    model = models.resnet18(pretrained=use_pretrained)
    set_parameter_requires_grad(model, feature_extract)
    num_ftrs = model.fc.in_features
    model.fc = nn.Linear(num_ftrs, num_classes)
    model.input_size = 224
    model.feature_extract = feature_extract
    model.model_name = 'ResNet18'

    def outputs_and_loss(data, criterion):
        outputs = model(data['inputs'])
        loss = criterion(outputs, data['labels'])
        return outputs, loss
    
    model.outputs_and_loss = outputs_and_loss
    
    return model


def alexnet(num_classes, feature_extract, use_pretrained=True):

    model = models.alexnet(pretrained=use_pretrained)
    set_parameter_requires_grad(model, feature_extract)
    num_ftrs = model.classifier[6].in_features
    model.classifier[6] = nn.Linear(num_ftrs, num_classes)
    model.input_size = 224
    model.feature_extract = feature_extract
    model.model_name = 'AlexNet'

    def outputs_and_loss(data, criterion):
        outputs = model(data['inputs'])
        loss = criterion(outputs, data['labels'])
        return outputs, loss
    
    model.outputs_and_loss = outputs_and_loss
    
    return model


def vgg11(num_classes, feature_extract, use_pretrained=True):
    
    model = models.vgg11_bn(pretrained=use_pretrained)
    set_parameter_requires_grad(model, feature_extract)
    num_ftrs = model.classifier[6].in_features
    model.classifier[6] = nn.Linear(num_ftrs, num_classes)
    model.input_size = 224
    model.feature_extract = feature_extract
    model.model_name = 'VGG11'
    
    def outputs_and_loss(data, criterion):
        outputs = model(data['inputs'])
        loss = criterion(outputs, data['labels'])
        return outputs, loss
    
    model.outputs_and_loss = outputs_and_loss
    
    return model


def squeezenet(num_classes, feature_extract, use_pretrained=True):

    model = models.squeezenet1_0(pretrained=use_pretrained)
    set_parameter_requires_grad(model, feature_extract)
    model.classifier[1] = nn.Conv2d(512, num_classes, kernel_size=(1, 1), stride = (1, 1))
    model.num_classes = num_classes
    model.input_size = 224
    model.feature_extract = feature_extract
    model.model_name = 'SqueezeNet'
    
    def outputs_and_loss(data, criterion):
        outputs = model(data['inputs'])
        loss = criterion(outputs, data['labels'])
        return outputs, loss
    
    model.outputs_and_loss = outputs_and_loss
    
    return model


def densenet121(num_classes, feature_extract, use_pretrained=True):

    model = models.densenet121(pretrained=use_pretrained)
    set_parameter_requires_grad(model, feature_extract)
    num_ftrs = model.classifier.in_features
    model.classifier = nn.Linear(num_ftrs, num_classes)
    model.input_size = 224
    model.feature_extract = feature_extract
    model.model_name = 'DenseNet121'
    
    def outputs_and_loss(data, criterion):
        outputs = model(data['inputs'])
        loss = criterion(outputs, data['labels'])
        return outputs, loss
    
    model.outputs_and_loss = outputs_and_loss
    
    return model


def inception(num_classes, feature_extract, use_pretrained=True):

    model = models.inception_v3(pretrained=use_pretrained, aux_logits=True)
    set_parameter_requires_grad(model, feature_extract)
    # Handle the auxilary net
    num_ftrs = model.AuxLogits.fc.in_features
    model.AuxLogits.fc = nn.Linear(num_ftrs, num_classes)
    # Handle the primary net
    num_ftrs = model.fc.in_features
    model.fc = nn.Linear(num_ftrs, num_classes)
    model.input_size = 299
    model.feature_extract = feature_extract
    model.model_name = 'Inception'
    
    def outputs_and_loss(data, criterion):
        outputs, aux_outputs = model(data['inputs'])
        loss1 = criterion(outputs, data['labels'])
        loss2 = criterion(aux_outputs, data['labels'])
        loss = loss1 + 0.4*loss2
        return outputs, loss
    
    model.outputs_and_loss = outputs_and_loss
    
    return model

