import torch
import copy
import time
import pandas as pd
import pytorch_hymenoptera.libs.model.serializer.pytorch_file as pt_file
import pytorch_hymenoptera.ui as ui
import pytorch_hymenoptera.libs.util as util


class Recorder():
    
    records = []
    best = {
        'epoch': 0,
        'phase': 0,
        'loss': 0,
        'acc': 0,
    }
    
    def __init__(self, model_name):
        self.model_name = model_name
        self._since = time.time()
    
    def end(self):
        self.total_time = time.time() - self._since
    
    def add(self, epoch, phase, loss, acc, class_mtcs):
        
        record = {
            'epoch': epoch,
            'phase': phase,
            'loss': loss,
            'acc': acc,
        }
        
        for k1, mtcs in class_mtcs.items():
            for k2, mtx in mtcs.items():
                record[k1 + '_' + k2] = mtx
                
        self.records.append(record)
        # print(record)
        if phase == 'val' and acc > self.best['acc']:
            self.best = {k: v for k, v in record.items() if not k == 'phase'}
        # print(self.records[len(self.records) - 1])
        
    def save(self, serial_prefix='N', prefix_gen=lambda: ''):
        # summary
        with open(ui.warehouse_('insight/hym_train_records.csv'), 'a') as f_hym:
            heads = 'serial_number,git_sha,model_name,save_time,total_train_time,'
            values = '{},{},{},{},{},'.format(
                serial_prefix,
                ui.git_sha()[:7],
                self.model_name,
                ui.now_str(),
                util.format_elapsed_time(self.total_time)
            )
            for key, value in self.best.items():
                heads += key + ','
                if isinstance(value, float):
                    values += '{:4f},'.format(value)
                # elif isinstance(value, int):
                #     contents += '{:n},'.format(value)
                else:
                    values += '{},'.format(value)
            f_hym.write(heads + '\n' + values + '\n')
            
        # detailed training records
        with open(
            ui.warehouse_loc_name(
                'insight',
                '{}{}_train_records.csv'.format(prefix_gen(), self.model_name)
            ),
            'x'
        ) as f_rcds:
            contents = ''
            for key in self.records[0]:
                contents += key + ','
            
            contents += '\n'
            
            for record in self.records:
                for key, value in record.items():
                    if isinstance(value, float):
                        contents += '{:4f},'.format(value)
                    # elif isinstance(value, int):
                    #     contents += '{:n},'.format(value)
                    else:
                        contents += '{},'.format(value)
                contents += '\n'
            f_rcds.write(contents)
            
    def df_records(self):
        return pd.Dataframe(self.records)


def train(model,
          data_obj,
          criterion,
          optimizer,
          num_epochs=25,
          device='cpu',
          serial_prefix='N',
          prefix_gen=lambda: ''):
    
    best = {
        'epoch': 0,
        'acc': 0.0,
        'model_wts': copy.deepcopy(model.state_dict())
    }
    recorder = Recorder(model.model_name)
    
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch + 1, num_epochs))
        print('-' * 10)
        
        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode
                
            running_loss = 0.0
            running_corrects = 0
            confusions = {
                key: {
                    'tp': 0,
                    'fp': 0,
                    'fn': 0
                }
                for key in data_obj.class_idx
            }
            
            # Iterate over data.
            for inputs, labels in data_obj.loader[phase]:
                data = {
                    'inputs': inputs.to(device),
                    'labels': labels.to(device)
                }
                
                # zero the parameter gradients
                optimizer.zero_grad()
                
                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    outputs, loss = model.outputs_and_loss(data, criterion)
                    _, preds = torch.max(outputs, 1)
                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                        
                # statistics
                running_loss += loss.item() * data['inputs'].size(0)
                running_corrects += torch.sum(preds == data['labels'].data)
                for x, y in zip(data['labels'].data, preds):
                    x = data_obj.idx_to_class(x.item())
                    y = data_obj.idx_to_class(y.item())
                    if x == y:
                        confusions[x]['tp'] += 1
                    else:
                        confusions[x]['fn'] += 1
                        confusions[y]['fp'] += 1
                        
            epoch_loss = running_loss / len(data_obj.loader[phase].dataset)
            epoch_acc = running_corrects.double() / len(
                data_obj.loader[phase].dataset)
            class_matrices = {
                key: {
                    'precision': util.dvd(cnts['tp'], cnts['tp'] + cnts['fp']),
                    'recall': util.dvd(cnts['tp'], cnts['tp'] + cnts['fn']),
                }
                for key, cnts in confusions.items()
            }
            for _, stats in class_matrices.items():
                stats['f1'] = util.dvd(
                    2 * stats['precision'] * stats['recall'],
                    stats['precision'] + stats['recall'])
                
            # epoch , phase, loss, acc, precision/recall/f1 of classes.
            recorder.add(epoch + 1, phase, epoch_loss, epoch_acc.item(),
                         class_matrices)
            print('epoch: {}, phase: {} Loss: {:.4f} Acc: {:.4f}'.format(
                epoch + 1, phase, epoch_loss, epoch_acc, class_matrices))
            
            # deep copy the model
            if phase == 'val' and epoch_acc > best['acc']:
                best['epoch'] = epoch + 1
                best['acc'] = epoch_acc
                best['model_wts'] = copy.deepcopy(model.state_dict())
                
    recorder.end()
    
    # serialize weights
    if best['epoch'] == num_epochs:
        pt_file.write(
            model.state_dict(), 'modelstore',
            '{}{}_best_last.pt'.format(prefix_gen(), model.model_name))
    else:
        pt_file.write(best['model_wts'], 'modelstore',
                      '{}{}_best.pt'.format(prefix_gen(), model.model_name))
        pt_file.write(model.state_dict(), 'modelstore',
                      '{}{}_last.pt'.format(prefix_gen(), model.model_name))
        
    # serialize records
    recorder.save(serial_prefix, prefix_gen)
    
    print('Training complete in {:.0f}m {:.0f}s'.format(
        recorder.total_time // 60, recorder.total_time % 60))
    print('Best val Acc: {:4f}'.format(best['acc']))


def params_to_update(model):
    if model.feature_extract:
        params_to_update = []
        for name, param in model.named_parameters():
            if param.requires_grad:
                params_to_update.append(param)
                print("\t", name)
    else:
        params_to_update = model.parameters()
        for name, param in model.named_parameters():
            print("\t", name)
    return params_to_update
