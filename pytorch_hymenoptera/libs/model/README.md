# Notice:

User who established the model needs to finish this document so that a trained model could be well indicated the usage.

This block and the following text for tutorials should be eliminated after you finish this README.md.



# Training data

In this part, the user should declare that what data is used during training.
(Do you use any dataset or other data?)



# Environment

In this part, the user should declare the model's training environment.
What kind of environment settings do you use when training your model?
Tensorflow? PyTorch? Or others?
It is strongly recommended that the user also describe the model's dependencies and versions. 



# Model

In this part, the user should clarify the following questions:
What's your model name?
What's your model format? 
(Save/Load should be handled at the model/serializer directory in a dong project)

What's your model structure? (Inception? Vgg? Resnet? Mobilenet? Or it's your own design?)
(The model structure should be declared at the model/init directory)

What's your model's input? In what format?
What's your model's inferencing output? In what format?
(You should provide an example code for user to easily load and user your model)

Is your model frozen or not?
(If your model is NOT frozen, then you should provide the structure and usage for each file)



### *Plus

It would be great if you also provides your testing result on your model so that people can know if they get a reasonable result when using your model inferencing.