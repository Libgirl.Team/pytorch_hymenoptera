import pytorch_hymenoptera.libs.model.train.pytorch_train as trn_bld


# def build_model(model):    
#     model.train = trn_bld.train
#     model.params_to_update = trn_bld.params_to_update
#     print('Model Built!')
#     return model


def set_parameter_requires_grad(model, feature_extracting):
    if feature_extracting:
        for param in model.parameters():
            param.requires_grad = False
