import os
import torch
from torchvision import datasets, transforms
import pytorch_hymenoptera.ui as ui


class Hymenmoptera():
    def __init__(self, input_size, batch_size):

        data_dir = ui.warehouse_('rawdata/hymenoptera_data')

        # Data augmentation and normalization for training
        # Just normalization for validation
        data_transforms = {
            'train':
            transforms.Compose([
                transforms.RandomResizedCrop(input_size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(
                    [0.485, 0.456, 0.406],
                    [0.229, 0.224, 0.225],
                )
            ]),
            'val':
            transforms.Compose([
                transforms.Resize(input_size),
                transforms.CenterCrop(input_size),
                transforms.ToTensor(),
                transforms.Normalize(
                    [0.485, 0.456, 0.406],
                    [0.229, 0.224, 0.225],
                )
            ]),
        }

        print("Initializing Datasets and Dataloaders...")

        # Create training and validation datasets
        image_datasets = {
            x: datasets.ImageFolder(os.path.join(data_dir, x),
                                    data_transforms[x])
            for x in ['train', 'val']
        }

        # Create training and validation dataloaders
        self.loader = {
            x: torch.utils.data.DataLoader(image_datasets[x],
                                           batch_size=batch_size,
                                           shuffle=True,
                                           num_workers=4)
            for x in ['train', 'val']
        }

        classes = os.listdir(os.path.join(data_dir, 'val'))
        classes.sort()

        self.class_idx = {classes[i]: i for i in range(len(classes))}

    def idx_to_class(self, idx):

        for name, number in self.class_idx.items():
            if idx == number:
                return name

        raise ValueError('index not in the dictionary of sample classes!')
        
    def blabla(self):
        return 'Hymenmoptera'
