import os
import git
from datetime import datetime
from pathlib import Path


def now_str():
    return datetime.now().strftime("%m/%d/%Y_%H:%M:%S")


cfd = Path(__file__).parent
repo = git.Repo(cfd, search_parent_directories=True)
sha = repo.head.object.hexsha


def git_sha():
    return sha


def warehouse_(path):
    return os.path.join(os.environ.get('WAREHOUSE_PATH'), path)


def warehouse_loc_name(location, name):
    return os.path.join(os.environ.get('WAREHOUSE_PATH'), location, name)


def serial_prefix():
    
    with open(os.path.join(os.environ.get('WAREHOUSE_PATH'), 'serial_number'),
              'r') as srl_file:
        n = srl_file.read()
        
    with open(os.path.join(os.environ.get('WAREHOUSE_PATH'), 'serial_number'),
              'w') as srl_file:
        srl_file.write(str(int(n) + 1))
        
    return n


def serial_generator(pre='N'):
    n = 0
    while True:
        yield '{}_{}_'.format(pre, n)
        n += 1


def get_gnxt(pre=None):
    gen = serial_generator(pre)
    
    def gnxt():
        return next(gen)
    
    return gnxt
