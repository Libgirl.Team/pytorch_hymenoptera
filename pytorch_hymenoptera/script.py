import torch
import torch.nn as nn
import torch.optim as optim
import pytorch_hymenoptera.libs.model.init.initializer as m_init
from pytorch_hymenoptera.libs.data.hymennoptera import Hymenmoptera as Hym
import pytorch_hymenoptera.libs.model.train.pytorch_train as pt_trn
import pytorch_hymenoptera.ui as ui
from pathlib import Path


def train_script(get_model, num_epochs=1):
    
    # prefix generator function for naming files
    srl = ui.serial_prefix()
    gen = ui.get_gnxt(srl)
    
    # Detect if GPU available
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print('device: {}.'.format(device))
    
    # Number of classes in the dataset
    num_classes = 2
    
    # Batch size for training (change depending on how much memory you have)
    batch_size = 8
    
    # Flag for feature extracting. When False, we finetune the whole model,
    #   when True we only update the reshaped layer params
    feature_extract = True
    
    model = get_model(num_classes, feature_extract).to(device)
    data = Hym(model.input_size, batch_size)
    optimizer = optim.SGD(pt_trn.params_to_update(model),
                          lr=0.001,
                          momentum=0.9)
    
    # Setup the loss fxn
    criterion = nn.CrossEntropyLoss()
    
    # Train and evaluate
    pt_trn.train(model,
                 data,
                 criterion,
                 optimizer,
                 num_epochs,
                 device,
                 prefix_gen=gen,
                 serial_prefix=srl)
    
    # ui.write_time('end')


def main():
    train_script(m_init.resnet18())
    train_script(m_init.alexnet())
